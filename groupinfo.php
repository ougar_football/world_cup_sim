<?php

require_once("kdb.inc");

$dbh=MyDatabase::connect("vm");
$season=date("Y")+(date("m")>7);

// Fetch all matches
$q="select pgram.kampid as id,'VM' as t,substr(runde,8,1) as l,hjemme as tid,ude as oid,hjemmemaal as gf,udemaal as ga from pgram left join res on pgram.kampid=res.kampid where aar=2018 order by l,dato";
$res=$dbh->kquery($q);
$matches=array();
while ($match=$res->fetch_assoc()) {
  if (!array_key_exists($match['t'],$matches))
    $matches[$match['t']]=array();
  if (!array_key_exists($match['l'],$matches[$match['t']]))
    $matches[$match['t']][$match['l']]=array();
  $matches[$match['t']][$match['l']][]=$match;
}
$jsonmatch=json_encode($matches);

// Make list of all teams with team info and current group standings;
$q="select \"VM\" as t,gruppe as g,id,stilling.land as n,fork as c,0 as pot,matches as m,win as w,draw as d,lose as l,points as p,goalf as f,goala as a,pos,0 as tc,0 as cc from stilling where aar=2018";
$res=$dbh->kquery($q);
$teams=array();
while ($t=$res->fetch_assoc()) {
  if (!array_key_exists($t['t'],$teams))
    $teams[$t['t']]=array();
  if (!array_key_exists($t['g'],$teams[$t['t']]))
    $teams[$t['t']][$t['g']]=array();
  $teams[$t['t']][$t['g']][$t['id']]=$t;
}
$jsonteam=json_encode($teams);

print("// This file has data for the World Cup 2018 group stage\n".
      "// -------------------------------------------------------------------------------------\n".
      "// It consists of 2 objects. Teams and Matches\n".
      "//\n".
      "// Teams:\n".
      "// Outer object has the value 'VM'\n".
      "// Next level has the 8 groupletters as index.\n".
      "// Next level has 4 entries, one for each team, team id is index.\n".
      "// So to get info of Russia: Teams.VM.A[\"60\"]\n".
      "// Since I work on 1 group at a time, and get the team ids from the matchlist\n".
      "// this structure makes it easy to get info for a given team.\n".
      "// Innermost object has all team info:\n".
      "// t: tournament, g: groupletter, id: team id, n: team name, c: team country abbr,\n".
      "// pot: seeding pot, m: num matches, w: won, d: drawn, l: lost, p: points,\n".
      "// f: goals for, a: goals against, pos: current group position,".
      "// tc: team uefa coefficient, cc: country uefa coefficient\n".
      "//\n".
      "// Matches:\n".
      "// All match info from this years group stage in EL and CL, organized in the same\n".
      "// way as above, with tournament and group number as first indices.\n".
      "// Last level is info about the 12 matches, indexed from 0 to 11. Available fields:\n".
      "// id: matchid, t: tournament, r: round (1-6), l: groupletter, tid: id of hometeam,\n".
      "// oid: id of awayteam, gf: home goals (unplayed=null), ga: awaygoals (unplayed=null)\n\n");
print("var Teams=$jsonteam\n\n");
print("var Matches=$jsonmatch\n");
?>
