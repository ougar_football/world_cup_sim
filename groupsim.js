// Read all get parameters
// var params={};window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(str,key,value){params[key] = value;});

// Class to calculate odds and goal distributions for a match given the difference in team strengts
var MatchProb=new MatchProbability();
MatchProb.basehome = 1.60; // In europe hometeams score more goals than default settings indicate
MatchProb.baseaway = 1.10; // ... and awayteams score fewer goals

// Translate tournament short names into full names without ugly if-statements
var TournamentNames={"VM":"Verdensmesterskaberne"};

// Definition of variables for the currently selected group
var CTournament; // EL or CL
var CGroup;      // Group letter
// List of the teams and matches of the current tournament+group considered
var CTeams;
var CMatches;

// Time simulations
var timeStart;

// When document is loaded, setup the needed event handlers
$(document).ready(function(){
  // Make tabs
  $("#tabs").tabs();
  // When a cl or el table is clicked, open that group in the 3rd tab
  $("#tab-vm").find("table").click(openGroup);

  // When "Run" button is pressed, start the simulation
  $("#action button").click(runSimulation);

  // When questionmarks are clicked, show the corresponding help dialog windows
  $("#helpmatch").click(helpmatch);
  $("#helptable").click(helptable);
  $("#helpaction").click(helpaction);
  $("#helpresult").click(helpresult);
  $("#helpchange").click(helpchange);
  $("#helpskill").click(helpskill);
  $("#helpstrength").click(helpstrength);

  $("#matchlist img").click(rotateMatch);
});

// When a group is selected, open a simulation tab for that group
function openGroup(e) {
  // Get title of clicked table where tournament and group is found
  var title=$(this).find("th:first").text().trim();
  // If only two tabs exists, then open a 3rd one
  if ($('#tabs >ul >li').length<2) newtab();
  // Set copy table header to new tab title
  $("#tabs >ul >li >a:last").text(title);
  // Find selected tournament and group
  CTournament=title.substr(0,2);
  CGroup=title.substr(12,1);
  console.log("Tour:  "+CTournament);
  console.log("Group: "+CGroup);
  // Set h2 headline
  $("#tab-calc h2").text("VM - Gruppe "+CGroup);
  // Select the matches and teams for this group from the global lists of all groups
  CMatches=Matches[CTournament][CGroup];
  CTeams=Teams[CTournament][CGroup];
  // Load all info for this group into the DOM on the tab-calc tab
  makeCurrentStandings();
  makeTeamStrength();
  initRes();
  showResult();
  populateMatchList();
  // Setup the progressbar
  $("#progressbar").progressbar({value: 0});

  // Set tab active (always the 2nd - we only create a new the first time)
  $("#tabs").tabs("option","active",1);
}

function initRes() {
  // Create result property for all teams, if they do not exist (initiate)
  for (var tid in CTeams)
    if (!CTeams[tid].hasOwnProperty("results")) {
      CTeams[tid].results=false;
      CTeams[tid].rescomp=false;
    }
}

// Create the 3rd tab - the tab-calc tab.
function newtab(title) {
  var ul = $("#tabs").find( "ul" );
  $("<li><a href='#tab-calc'>New tab</a></li>").appendTo(ul);
  $("#tabs").tabs("refresh");
  //$("#tab-calc").load("/group_stage_simulation/newtab.html");
}

// Run through all 12 matches, and insert teams and results
// Calculat
function populateMatchList() {
  $("#matchlist table tr").each(function(i,row) {
    if (i==0) return;// Skip table header
    var match=CMatches[i-1];
    var tds=$(row).find("td");
    // Insert home team name and title attribute
    tds.eq(1).html(CTeams[match.tid].n);
    tds.eq(1).attr("title",CTeams[match.tid].n+": Team coef="+CTeams[match.tid].tc+", Country coef="+CTeams[match.tid].cc+", Skill="+CTeams[match.tid].skill.toFixed(1));
    // Insert away team name and title attribute
    tds.eq(3).html(CTeams[match.oid].n);
    tds.eq(3).attr("title",CTeams[match.oid].n+": Team coef="+CTeams[match.oid].tc+", Country coef="+CTeams[match.oid].cc+", Skill="+CTeams[match.oid].skill.toFixed(1));
    // Calculate odds from team skills
    match.odds=MatchProb.MatchOdds(CTeams[match.tid].skill-CTeams[match.oid].skill);
    // Fetch the calculated accumulated goal-probabilities used to determine a random result when simulating
    match.phomeacc=MatchProb.phomeacc.slice(); // Use slice to COPY array and
    match.pawayacc=MatchProb.pawayacc.slice(); // not just make a reference
    // Create user defined odds, if they have not been created. Make them by rounded percentages from autoOdds
    if (!match.hasOwnProperty("userOdds"))   match.userOdds=match.odds.slice().map(function(q) {return(Math.round(100*parseFloat(q)));});
    // Create object for user defined result, if it has not been defined for this match before. Default is 0-0
    if (!match.hasOwnProperty("userResult")) {
      if (match.gf!==null) match.userResult=[match.gf,match.ga];
      else                 match.userResult=["0","0"];
    }
    // Set initial state of match
    // If there is actual result, always return to that, no matter if something else was chosen before.
    if (match.gf!==null) setMatchResult(tds,match);
    // If match state was set to userOdds or userResults previously, go with that
    else if (match.hasOwnProperty("state")) {
      if (match.state=="userresult") setMatchUserResult(tds, match, 3);
      if (match.state=="userodds")   setMatchUserOdds(tds, match, 3);
    }
    // If there is no result, and no previous state, use "autoodds"
    else setMatchAutoOdds(tds, match, 3);
  });
}

function rotateMatch(e) {
  var img=$(this);
  var tds=$(this).parent().parent().children();
  var match=CMatches[$(this).parent().parent().index()-1];

  if (match.state == "autoodds")
    setMatchUserOdds(tds, match);
  else if (match.state == "userodds") {
    if (match.gf!==null) setMatchResult(tds,match);
    // If match is not played (no result) skip matchResult in rotation
    else setMatchUserResult(tds,match);
  }
  else if (match.state == "result")
    setMatchUserResult(tds,match);
  else if (match.state == "userresult")
    setMatchAutoOdds(tds, match);
  else alert("FAIL");
}

function circlenum(match) {
  if (match.gf===null) return("3");
  else return("");
}

function setMatchResult(tds, match) {
  tds.eq(4).html("<span class='goal1'>"+match.gf+"</span>");
  tds.eq(5).html("-");
  tds.eq(6).html("<span class='goal2'>"+match.ga+"</span>");
  tds.eq(7).find("img").attr("src","img/rotate_bottom.png");
  tds.eq(7).find("img").attr("title","Actual results selected. Click to rotate.");
  // State of match is now "result"
  match.state="result";
}

function setMatchAutoOdds(tds, match) {
  tds.eq(4).html("<input value='"+(100*match.odds[0]).toFixed(0)+"' disabled>");
  tds.eq(5).html("<input value='"+(100*(1-match.odds[0].toFixed(2)-match.odds[2].toFixed(2))).toFixed(0)+"' disabled>");
  tds.eq(6).html("<input value='"+(100*match.odds[2]).toFixed(0)+"' disabled>");
  tds.eq(7).find("img").attr("src","img/rotate_top"+circlenum(match)+".png");
  tds.eq(7).find("img").attr("title","Automatic odds selected. Click to rotate.");
  // State of match is now "autoodds"
  match.state="autoodds";
}

function setMatchUserOdds(tds, match) {
  tds.eq(4).html("<input value='"+match.userOdds[0]+"'>");
  tds.eq(5).html("<input value='"+match.userOdds[1]+"'>");
  tds.eq(6).html("<input value='"+match.userOdds[2]+"'>");
  tds.eq(7).find("img").attr("src","img/rotate_right"+circlenum(match)+".png");
  tds.eq(7).find("img").attr("title","User defined odds selected. Click to rotate.");
  // State of match is now "autoodds"
  match.state="userodds";
  tds.find("input").on("change",saveUserOdds);
}

function setMatchUserResult(tds, match) {
  tds.eq(4).html("<input class='res' value='"+match.userResult[0]+"'>");
  tds.eq(5).html("-");
  tds.eq(6).html("<input class='res' value='"+match.userResult[1]+"'>");
  tds.eq(7).find("img").attr("src","img/rotate_left"+circlenum(match)+".png");
  tds.eq(7).find("img").attr("title","User chosen results selected. Click to rotate.");
  // State of match is now "autoodds"
  match.state="userresult";
  tds.find("input").on("change",saveUserResult);
}

function saveUserResult() {
  var mid=$(this).parent().parent().index()-1;
  var value = $(this).val().trim();
  // Check if input is valid - show red border if input is not OK
  if (!value.match(/^\d\d?$/)) {
    $(this).css({"box-shadow": "0 0 4px #FF0000"});
    $("#action button").html("Error").prop('disabled', true);
  }
  // If input is OK - save
  else {
    $(this).css({"box-shadow": "none"});
    $("#action button").html("Run").prop('disabled', false);
    value=parseInt(value);
  }
  // Find out if hometeam (5th td) or awayteam (7th td) was changed
  var homeaway=0;
  if ($(this).parent().index()==6) homeaway=1;
  // Save value in match arrey
  CMatches[mid].userResult[homeaway]=value;
}

function saveUserOdds() {
  var mid=$(this).parent().parent().index()-1;
  var value = $(this).val();
  var index = $(this).parent().index()-4;
  // Check if input is valid - show red border if input is wrong
  if (!value.match(/^\d+\.?\d*$/)) {
    $(this).css({"box-shadow": "0 0 4px #FF0000"});
    $("#action button").html("Error").prop('disabled', true);
  }
  // If input is OK - save
  else {
    $(this).css({"box-shadow": "none"});
    $("#action button").html("Run").prop('disabled', false);
    value=parseFloat(value);
  }
  // Check that all fields are not 0
  CMatches[mid].userOdds[index]=value;
  var sum = CMatches[mid].userOdds.reduce(function(a, b) { return a + b; }, 0);
  if (sum==0) {
    $("#action button").html("Error").prop('disabled', true);
  }
}  

// Read the odds from the matchlist table. Check if the values have changed compared to the
// automatically caluclated odds.
function readUserOdds() {
  var oddsError="";
  $("#matchlist table tr").each(function(i,row) {
    if (i==0) return;                   // Skip header
    var match=CMatches[i-1];            // Get match info for this match
    var inputs=$(row).find("input");    // Get all inputs from this matchrow
    if (inputs.length==0) return;       // No input fields. Use result
    if (inputs.length==2) return;       // 2 input fields - result is specified - use that result - NOT IMPLEMENTED
    if (inputs.length==3) {             // 3 input fields - odds spcified. Check if it is the standard odds
      var userOdds=[
        parseFloat(inputs.eq(0).val()), // Read relative probability for the 3 
        parseFloat(inputs.eq(1).val()), // outcomes of the match
        parseFloat(inputs.eq(2).val())
      ];
      // Normalize odds so sum equals 1
      var sum=userOdds[0]+userOdds[1]+userOdds[2];
      // Check that sum is greater than 0 and that no odds are negative
      if (!(sum>0 && userOdds[0]>=0 && userOdds[1]>=0 && userOdds[2]>=0)) {
        // Give alert and return false, which cancels calculation in runSimulation
        oddsError+="Invalid odds for match "+i+"\n";
      }
      userOdds[0]=userOdds[0]/sum;
      userOdds[1]=userOdds[1]/sum;
      userOdds[2]=userOdds[2]/sum;
      // If input is changed, use new userOdds in stead of precalculated
      if (Math.abs(userOdds[0]-match.odds[0])>0.01 ||
          Math.abs(userOdds[1]-match.odds[1])>0.01 ||
          Math.abs(userOdds[2]-match.odds[2])>0.01) {
        match.userOdds=userOdds;
      }
    }
  });
  if (oddsError) {alert(oddsError); return(false);}
  else return(true);
}

function makeCurrentStandings() {
  var rows=$("#currentpos table tr");
  rows.eq(3).attr("class",CTournament.toLowerCase()+"3rd");
  for (var tid in CTeams) {
    // Find all tds in the "pos" row - pos i the current position of this team
    var tds=rows.eq(CTeams[tid].pos).find("td");
    // Set team name and update image to correct country
    tds.eq(0).html(CTeams[tid].n);
    var cimg=tds.eq(1).find("img");
    cimg.attr("src","img/"+CTeams[tid].c.toLowerCase()+".png");
    cimg.attr("title",CTeams[tid].c);
    //tds.eq(2).html(teams[tid].c);
    tds.eq(2).html(CTeams[tid].m);
    tds.eq(3).html(CTeams[tid].w+"-"+CTeams[tid].d+"-"+CTeams[tid].l);
    tds.eq(4).html(CTeams[tid].p);
    tds.eq(5).find("span").eq(0).html(CTeams[tid].f);
    tds.eq(5).find("span").eq(1).html(CTeams[tid].a);
    setSkill(tid);
  }
}

// Run through all teams from this group and insert strength data in teamstrength table
//   Setup listener for skill-change 
function makeTeamStrength() {
  var rows=$("#teamstrength table tr");
  for (var tid in CTeams) {
    // Find all tds in the "pos" row - pos i the current position of this team
    var tds=rows.eq(CTeams[tid].pos).find("td");
    // Set team name and update image to correct country
    tds.eq(0).html(CTeams[tid].n);
    var cimg=tds.eq(1).find("img");
    cimg.attr("src","img/"+CTeams[tid].c.toLowerCase()+".png");
    cimg.attr("title",CTeams[tid].c);
    //tds.eq(2).html(teams[tid].c);
    tds.eq(2).html(CTeams[tid].tc);
    tds.eq(3).html(CTeams[tid].cc);
    tds.eq(4).html(CTeams[tid].skill);
    var input=tds.eq(5).find("input").eq(0);
    input.val(CTeams[tid].skill);
    // Add tid as custom data field to each range input
    input.data("tid", tid);
    //input.on("input",updateSkill.bind(null,tid)); 
    input.on("change",slidestop);
    input.bind("input",function(e) {
      var newskill=parseFloat($(this).val()).toFixed(1);
      $(this).parent().prev().html(newskill);
      updateAutoOdds($(this).data("tid"), newskill);
    });
  }
}

// Called when skills are updated and autoodds for all matches should be recalculated
function updateAutoOdds(tid, skill) {
  // Set skill of team in team array
  CTeams[tid].skill=skill;
  // Run through all matches
  $("#matchlist table tr").each(function(i,row) {
    if (i==0) return;
    // Get current match ánd tds from this row
    var match=CMatches[i-1];
    var tds=$(row).find("td");
    // Calculate odds from team skills
    // No need to do anything unless this specific teams in involved
    if (match.tid==tid || match.oid==tid) {
      match.odds=MatchProb.MatchOdds(CTeams[match.tid].skill-CTeams[match.oid].skill);
      // Fetch the calculated accumulated goal-probabilities used to determine a random result when simulating
      match.phomeacc=MatchProb.phomeacc.slice(); // Use slice to COPY array and
      match.pawayacc=MatchProb.pawayacc.slice(); // not just make a reference
      if (match.state == "autoodds") {
        setMatchAutoOdds(tds, match);
        $(row).find("input").css({"background-color": "lightblue"});
      }
    }
  });
}

// Når der ikke slides mere med en skill-fætter, så nulstil farver efter 500ms
function slidestop() {
  setTimeout(function() {
    $("#matchlist input").css({"background-color": "white"});
  }, 800);
}

function setSkill(tid) {
  var countryCoef=CTeams[tid].cc;
  var teamCoef=CTeams[tid].tc;
  teamCoef-=Math.max(0,teamCoef-70)/2;
  countryCoef-=Math.max(0,countryCoef-50)/2;
  CTeams[tid].skill=Math.round(parseFloat(countryCoef)+parseFloat(teamCoef))/10;
}

function checkValidInput() {
  return(true);
}

function runSimulation() {
  //dumpSimulationSetup();
  if (!checkValidInput()) return;
  // Reset result array for each team and save
  for (var tid in CTeams) {
    CTeams[tid].rescomp=CTeams[tid].results;
    CTeams[tid].results=[0,0,0,0,0];
  }
  // Find number of simulations from input field
  var nSimulations=parseInt($("#action input").val());
  if (nSimulations<1) {alert("You need to specify number of simulations!"); return;}
  // Read odds from input fields - if false then just return due to odds error
  if (!readUserOdds()) return;
  // Disable run button
  $("#action button").html("Wait...").prop('disabled', true);
  timeStart=new Date();
  // Start the loop function, with total simulations and how many allready done: 0
  loopSim(nSimulations,0);
}

// Run the main loop. Don't run it all at once, since we want to show the progress
function loopSim(nSimulations,currentSim) {
  // So run this many simulations between each DOM update (1% at a time, but minimum 500)
  // And also maximum the number of iterations left, before we reach nSimulations
  var simBlock=Math.min(nSimulations-currentSim,Math.max(500,Math.round(nSimulations/100)));
  // Main calculation loop
  for (var nSim=0;nSim<simBlock;nSim++) {
    result=runOnce();   // Run all matches a single time
    for (var n=0;n<4;n++) {                          // Record results of this single run
      CTeams[result[n].tid].results[n]++;            // This team ended on place n, so increment global result
      CTeams[result[n].tid].results[4]+=result[n].p; // And this team got this many points, so increment points counter
    }
  }
  var partCompleted=(currentSim+simBlock)/nSimulations;
  var timeEllapsed=new Date()-timeStart;
  var timeLeft=timeEllapsed*(nSimulations/(currentSim+simBlock)-1);

  // Update the progress bar
  // Textfield for percentage completed
  var progress=Math.round(100*partCompleted)+"%";
  // If we are done, replace text with "done"
  if (currentSim+simBlock>=nSimulations) progress="Done";
  $("#progressbar").progressbar("value",Math.round(100*partCompleted))
                   .children('.ui-progressbar-value')
                   .html(progress);
  // If we have NOT reached the desired number of simulations, schedule some more
  if (currentSim+simBlock<nSimulations) {
    setTimeout(function() {loopSim(nSimulations,currentSim+simBlock);},3);
    return;
  }

  // OK we are finished. We have reached nSimulations.
  // =================================================
  // Divide all totalt with number of simulations, to get correct avg/percentage
  for (tid in CTeams)
    for (var n=0;n<5;n++)
      CTeams[tid].results[n]=CTeams[tid].results[n]/nSimulations;

  // Update simulation counts
  $.ajax({url: "/group_stage_sim/simulationcount.php?simulations="+nSimulations, success: function(result){
    $("#simcount").html(result);
  }});

  // Update results table
  showResult();
}

function showResult() {
  // Make array with final results. I can't figure out to sort CTeams object, indexed with club ids.
  var finalres=[];
  for (tid in CTeams) finalres.push(CTeams[tid]);
  // Sort teams on total number of points
  finalres.sort(function(a,b) {return(b.results[4]-a.results[4]);});
  // Fill out result table
  var resrows=$("#results table tr");
  var histrows=$("#resultcompare table tr");
  var d=null;
  for (var n=0;n<4;n++) {
    // Find all tds in the "pos" row (n+1 to skip header)
    var td1=resrows.eq(n+1).find("td");
    var td2=histrows.eq(n+1).find("td");
    // Set team name and update image to correct country
    td1.eq(0).html(CTeams[finalres[n].id].n);
    td2.eq(0).html(CTeams[finalres[n].id].n);
    var cimg=td1.eq(1).find("img");
    cimg.attr("src","img/"+CTeams[finalres[n].id].c.toLowerCase()+".png");
    cimg.attr("title",CTeams[tid].c);
    cimg=td2.eq(1).find("img");
    cimg.attr("src","img/"+CTeams[finalres[n].id].c.toLowerCase()+".png");
    cimg.attr("title",CTeams[tid].c);
    // Insert results (unless results==false which is call at init)
    if (finalres[n].results) {
      td1.eq(2).html((finalres[n].results[4]).toFixed(2));
      for (d=0;d<4;d++)
        td1.eq(d+3).html((finalres[n].results[d]*100).toFixed(1)+"%");
    }
    // If there is a previous simmulation - calculate change
    if (finalres[n].rescomp) {
      var dif=[];
      for (d=0;d<=4;d++) dif[d]=finalres[n].results[d]-finalres[n].rescomp[d];
      td2.eq(2).html((dif[4]>=0?"+":"")+dif[4].toFixed(2));
      for (d=0;d<4;d++) {
        td2.eq(d+3).html((dif[d]>=0?"+":"")+(100*dif[d]).toFixed(1)+"%");
      }
    }
  }
  // Enable Run button again
  $("#action button").html("Run").prop('disabled', false);
}

function runOnce() {
  // Reset table
  var table={};
  for (var tid in CTeams) {
    //          Team id   pts   games won   draw  lose  goals f/a   awayg  results between teams
    table[tid]={"tid":tid,"p":0,"g":0,"w":0,"d":0,"l":0,"f":0,"a":0,"ag":0,"indbyrd":{},"ind3":0}
    for (var opp in CTeams) if (opp!=tid) table[tid].indbyrd[opp]=0;
  }
  var res;
  // Run through all matches
  for (var m=0;m<12;m++) {
    var match=CMatches[m];
    // Get real result / user defined result / auto odds result / user odds result depending on state
    if      (match.state=="result")     // Actual results from match played
      res=[match.gf,match.ga]; 
    else if (match.state=="userresult") // User defined result
      res=match.userResult;
    else if (match.state=="autoodds")   // Autogenerated odds
      res=MatchProb.MatchResultScore(match.phomeacc,match.pawayacc); // Auto odds: use goal distribution to get a result
    else if (match.state=="userodds")   // User defined odds
      res=getResult(MatchProb.MatchResultOdds(match.userOdds));
    else console.log("ABOOORT");
    // Print match results to console
    // console.log(pRight(CTeams[match.tid].n," ",16)+" - "+pRight(CTeams[match.oid].n," ",16)+" "+res[0]+"-"+res[1]);
    // Update win/draw/lose counts
    if      (res[0]>res[1]) {table[match.tid].w++; table[match.oid].l++;}
    else if (res[0]<res[1]) {table[match.oid].w++; table[match.tid].l++;}
    else                    {table[match.tid].d++; table[match.oid].d++;}
    // Update inbyrdes kampe (points->goaldif->goals->awaygoals
    table[match.tid].indbyrd[match.oid]+=3000000*(res[0]>res[1])+1000000*(res[0]==res[1])+10000*(res[0]-res[1])+100*res[0];
    table[match.oid].indbyrd[match.tid]+=3000000*(res[0]<res[1])+1000000*(res[0]==res[1])+10000*(res[1]-res[0])+101*res[1];
    // Update goalcounts
    table[match.tid].f+=res[0]; table[match.tid].a+=res[1];
    table[match.oid].f+=res[1]; table[match.oid].a+=res[0];
    // Update awaygoals
    table[match.oid].ag+=res[1];

    var ph=3*(res[0]>res[1])+(res[0]==res[1]);
    var pa=3*(res[0]<res[1])+(res[0]==res[1]);
  }
  // Update points and games
  for (tid in table) {
    table[tid].p=table[tid].d+3*table[tid].w;
    table[tid].g=table[tid].w+table[tid].d+table[tid].l;
  }
  // Find out the order of the teams based on the UEFA rules
  return(sortTable(table));
}

function dumpSimulationSetup() {
  for (m in CMatches) {
    match=CMatches[m];
    if      (match.state=="userresult") d=match.userResult;
    else if (match.state=="userodds")   d=match.userOdds.map(function(x){return(x.toFixed(2));});
    else if (match.state=="result")     d=[match.gf, match.ga];
    else if (match.state=="autoodds")   d=match.odds.map(function(x){return(x.toFixed(2));});
    else console.log("FAAAAIL: "+match.state);
    console.log("Match "+m+" ("+match.state+"): "+d);
  }
}

// This is the damn annoying one, because if clubs are even on points, it's the
// matches between the teams that counts first.
function sortTable(table) {
  ntable=[];
  for (var t in table) ntable.push(table[t]);
  ntable.sort(stdSortCmp);
  if (ntable[0].p==ntable[2].p || ntable[1].p==ntable[3].p) { // 3 or more teams are equal on points
    sortTable3equal(ntable);
    ntable.sort(stdSortCmp);
  }
  return(ntable);
}

function stdSortCmp(a,b) {
  if (a.p!=b.p)           return(b.p-a.p);         // Difference in points
  if (a.ind3!=b.ind3)     return(b.ind3-a.ind3);   // Matches between 3 teams equal on points (0 if not the case)
  if (a.indbyrd[b.tid] != b.indbyrd[a.tid]) return(b.indbyrd[a.tid]-a.indbyrd[b.tid]); // Matches between teams
  if (a.f-a.a != b.f-b.a) return(b.f-b.a-a.f+a.a); // Total goal difference
  if (a.f != b.f)         return(b.f-a.f);         // Total number of goals scored
  if (a.ag != b.ag)       return(b.ag-a.ag);       // Total away goals
  if (a.w != b.w)         return(b.w-a.w)          // Total victories
  return(Math.random()-.5); // Give up. Sort randomly and hope it will even out :-)
}

// Sort table if 3 or more teams are euqal on points - DAMN this is a mess
function sortTable3equal(ntable) {
  // If ALL 4 teams are equal on points, then just reset indbyrdes, and return to use the standard sorting function
  if (ntable[0].p==ntable[3].p) {
    for (var i=0;i<4;i++) {
      for (var id in ntable[i].indbyrd) ntable[i].indbyrd[id]=0;
    }
    return;
    /* NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE                              */
    /************************************************************************/
    /* Sorting algorithm is WRONG if all 4 teams are equal on points AND    */
    /* 2 or 3 teams (but not all 4) are equal on goaldif, goals scored and  */
    /* awaygoals scored. In this case we should remove the decided teams    */
    /* and return to evaluate the rest of the teams based on indbyrdes      */
    /* However, this has never been even CLOSE to happening                 */
  }
  // OK. 3 teams are equal on points - figure out the order of these 3 teams
  // -----------------------------------------------------------------------
  // Is it 1,2,3 that are equal or 2,3,4
  var s;
  if   (ntable[1].p==ntable[3].p) s=1; // 2,3,4 are equal on points
  else                            s=0; // 1,2,3 are equal on points
  // Get the team ids of the teams in question
  var tid0=ntable[s+0].tid;
  var tid1=ntable[s+1].tid;
  var tid2=ntable[s+2].tid;
  // Add the indbyrd values to a new total indbyrd value (ind3)
  ntable[s+0].ind3=ntable[s+0].indbyrd[tid1]+ntable[s+0].indbyrd[tid2];
  ntable[s+1].ind3=ntable[s+1].indbyrd[tid0]+ntable[s+1].indbyrd[tid2];
  ntable[s+2].ind3=ntable[s+2].indbyrd[tid0]+ntable[s+2].indbyrd[tid1];
  // If all 3 teams have same ind3 value, reset indbyrd, since indbyrd between 2 teams are irrelevant
  // If all teams do NOT have same value, keep indbyrd to use if 2 of the 3 has same ind3 value
  if (ntable[s+0].ind3==ntable[s+1].ind3 && ntable[s+0].ind3==ntable[s+2].ind3) {
    ntable[s+0].indbyrd[tid1]=0; ntable[s+0].indbyrd[tid2]=0;
    ntable[s+1].indbyrd[tid0]=0; ntable[s+1].indbyrd[tid2]=0;
    ntable[s+2].indbyrd[tid0]=0; ntable[s+2].indbyrd[tid1]=0;
  }
  // Return and sort by this ind3 value as second parameter.
  return;
}

// If user specifies odds, I don't know team strengts and can't calculate a
// random result with goals, which makes sense. In stead I choose a random
// homewin/draw/awaywin result. The result distribution are based on all
// european group matches since 2000.
// This ignores that a bad team who beats a good team, will probably NOT
// win by 5 goals, but the users often just choose to set the result by 
// setting 100% of chance to 1 result. In that case, there is nothing to
// base an educated result guess on, so I guess based on overall history.

function getResult(res) {
  var dice=Math.random();
  // Home win distribution from all group matches after 2000
  if (res=="1") {
    if      (dice<0.2419) return([1,0]);
    else if (dice<0.4219) return([2,0]);
    else if (dice<0.6116) return([2,1]);
    else if (dice<0.7109) return([3,0]);
    else if (dice<0.8124) return([3,1]);
    else if (dice<0.8629) return([3,2]);
    else if (dice<0.9062) return([4,0]);
    else if (dice<0.9351) return([4,1]);
    else if (dice<0.9537) return([4,2]);
    else if (dice<0.9601) return([4,3]);
    else if (dice<0.9767) return([5,0]);
    else if (dice<0.9949) return([5,1]);
    else                  return([5,2]);
  }
  else if (res=="x") {
    if      (dice<0.3021) return([0,0]);
    else if (dice<0.7812) return([1,1]);
    else if (dice<0.9674) return([2,2]);
    else                  return([3,3]);
  }
  else if (res=="2") {
    if      (dice<0.2419) return([0,1]);
    else if (dice<0.4219) return([0,2]);
    else if (dice<0.5212) return([0,3]);
    else if (dice<0.5645) return([0,4]);
    else if (dice<0.5811) return([0,5]);
    else if (dice<0.7708) return([1,2]);
    else if (dice<0.8723) return([1,3]);
    else if (dice<0.9012) return([1,4]);
    else if (dice<0.9194) return([1,5]);
    else if (dice<0.9699) return([2,3]);
    else if (dice<0.9885) return([2,4]);
    else if (dice<0.9936) return([2,5]);
    else                  return([3,4]);
  }
  else alert("WRONG!");
}

function printTable(t) {
  console.log("# Klub            G W-D-L P (F-A)");
  for (var s=0;s<4;s++)
    console.log((s+1)+" "+pRight(CTeams[t[s].tid].n," ",16)+t[s].g+" "+t[s].w+"-"+t[s].d+"-"+t[s].l+" "+t[s].p+" ("+t[s].f+"-"+t[s].a+")");
  console.log(" ");
}

function helpmatch() {
  help("List of group matches",
    "Here you can specify the probabilities or the results of all matches in the group. "+
    "<p>Each match can be specified in 1 of 4 ways:"+
    "<ul><li> The actual result of the game (default if played) "+
    "<li> User entered result "+
    "<li> Auto calculated probabilities (default if not played) "+
    "<li> User defined probabilities "+
    "</ul>"+
    "<p>When you specify probabilities, the 3 input boxes for each game, are "+
    "the predicted relative probabilities "+
    "for the outcome of the game: home-win, draw and away-win. So 50-20-30 means "+
    "50% chance of homewin, 20% for a draw and 30% for an away win. "+
    "<p>The numbers don't need to add up to 100. 5-2-3 or 10-4-6 is the same thing "+
    "- it's all relative :-)). "+
    "<p>You change between the 4 types of input (3 if game hasn't been played) by "+
    "clicking the 'rotate' button to the right.");
}

function helpaction() {
  help("What happens?","When you run the simulation, all the actual results as well "+
       "as user predicted results are added up and matches with odds are decided "+
       "randomly according to the odds. When all games are decided, we check who "+
       "ends up on which position in the group. The simulation is then run a number "+
       "of times (the number you enter) and we look at how often each club ends up "+
       "on each position in the group. This gives you an idea of each clubs chances "+
       "of winning the group or the chance to progress to the next round."+
       "<p>The games are decided with realistic scores. If automatic odds are used "+
       "the goal scores are determined randomly from the skill of the two teams. "+
       "If match is determined by user defined odds, we determine who wins or if it "+
       "is a draw, and then 'guess' a result based on distribution of scores of  "+
       "homewins/draws/awaywins of all european group matches since 2000.");
}

function helptable() {
  help("Current standings","I hope this is self explanatory");
}

function helpresult() {
  help("Result table","Here you can see the results of the simulation. "+
       "<p>The 'Avg P' show the average number of points each club ended up with "+
       "in the last simulation. You could easily calculate this number directly "+
       "from the match probabilities. "+
       "<p>The last 4 columns show the chance, that each club ends up on 1st-4th "+
       "place in the group. So each row and each column will add up to 100%. "+
       "<p>All UEFA rules are taken into account, when calculating the results. "+
       "If 2 or more teams are equal on points, the order is decided by first looking "+
       "at the matches between the teams in question. Every match has a simulated "+
       "result, distributed according to historic data, taking the probabilities into "+
       "account.");
}

function helpchange() {
  help("Result change table","Here you can see how much the results have changed "+
       "between the last 2 simulations. This makes it much easier to see the effect "+
       "of any changes made to the odds/results in the matchlist. "+
       "<p>For instance you can see how much a win in the next match improves the "+
       "chance of winning the group, etc. "+
       "<p>The columns show the change in points and percentage chance of ending "+
       "at each position in the group.");
}

function helpstrength() {
  help("Skill calculations","The estimated skill is just a number on a random scale, by default "+
       "calculated semi-arbitrarily "+
       "from the UEFA coefficients as the sum of the team and country coefficients divided "+
       "by 10 (and points above 70/50 are halved because big clubs/nations got too high "+
       "skill). <p>The skills of the hometeam and awayteam are then used to calculate the "+
       "probability of a win, a draw and a loss for each match in the group.");
}

function helpskill() {
  help("Skill adjusting","If you think the skill is wrong, you can adjust the skill using the "+
       "range slider. When you adjust the skill, the odds/probabilities of this teams matches "+
       "are automatically changed in the list of matches below. Matches changed are marked in "+
       "blue. Increasing the skill of a team increses the likehood, the the team will win the "+
       "matches.");
}

function help(title, text) {
  $("#helpdialog").html(text);
  $("#helpdialog").dialog({
    title:title
  });
}

function dump(thing) {
  console.log(JSON.stringify(thing));
}

// right padding s with c to a total of n chars
function pRight(s, c, n) {
  if (! s || ! c || s.length >= n) {
    return s;
  }
  var max = (n - s.length)/c.length;
  for (var i = 0; i < max; i++) {
    s += c;
  }
  return s;
}
