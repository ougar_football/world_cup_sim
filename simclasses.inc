<?php
require_once("baseclass.inc");

class Team extends Baseclass {
  public $id;
  public $name;
  public $country;
  public $skill;
  public $pot;
  public $matches;
  public $win;
  public $draw;
  public $lose;
  public $points;
  public $goalf;
  public $goala;
  public $pos;
  public $teamcoef;
  public $ccoef;
  public $season;
  public $tournament;
  public $groupletter;

  public function __construct($data) {
    parent::__construct($data);
    if ($this->name=="Borussia Mönchengladbach")
      $this->name="M'gladbach";
  }

  public function __toString() {
    $name=(strlen($this->name)>18?substr($this->name,0,17).".":$this->name);
    return(utf8_sprintf("%-18s (%s) %d %d-%d-%d %2d (%2d-%-2d)\n",$name,$this->country,$this->matches,$this->win,$this->draw,$this->lose,$this->points,$this->goalf,$this->goala));
  }

  public function tr() {
    $img="<img src='./img/".strtolower($this->country).".png' title='{$this->country}'>";
    $trclass=array("","vm1st","vm2nd","vm3rd","vm4th");
    //                             Club      Country Games   W- D- L      P       Mål
    return(utf8_sprintf("<tr class='%s'> <td> %-20s<td> %s <td> %d <td> %d-%d-%d <td> %d <td> <span class='goal1'>%2d</span>-<span class='goal2'>%2d</span>\n",
      $trclass[$this->pos],$this->name,$img,$this->matches,$this->win,$this->draw,$this->lose,$this->points,$this->goalf,$this->goala));
  }

  public function getSkill() {
    return($this->teamcoef+$this->ccoef);
  }
}

class Group extends Baseclass {
  public $season;
  public $tournament;
  public $groupletter;
  public $teams=array();

  public function __construct($season=null, $tournament=null, $groupletter=null) {
    $this->season=$season;
    $this->tournament=$tournament;
    $this->groupletter=$groupletter;
  }

  public function addTeam($data) {
    $this->teams[]=new Team($data);
  }

  public function load() {
    $q="select a.season,a.tournament,groupletter,a.team as id,pot,matches,win,draw,lose,points,goalf,goala,pos,b.name,b.country,c.coef as teamcoef,d.coef as ccoef ".
       "from groupres as a ".
       "join teams as b on a.team=b.id ".
       "join teamcoef as c on a.team=c.team and a.season=c.season ".
       "join countryrank as d on b.country=d.country and a.season=d.season ".
       "where a.season={$this->season} and tournament='{$this->tournament}' and groupletter='{$this->groupletter}' ".
       "order by tournament,groupletter,pos";
    $res=Database::get_handle()->kquery($q);
    while ($t=$res->fetch_assoc())
      $this->addTeam($t);
  }

  public function th() {
    return("<tr> <th> ".strtoupper($this->tournament)." - Gruppe {$this->groupletter} <th> <th> K <th> V-U-T <th> Point <th> Mål\n");
  }

  public function __toString() {
    $str=(sprintf("%-18s ( C ) G W-D-L  P (Goals)\n".
                   "-------------------------------------------\n",$this->tournament." - Group ".$this->groupletter));
    foreach ($this->teams as $t) $str.=$t->__toString();
    return($str);
  }
}

function getAllGroups($season) {
  $groups=array();
  $q="select aar as season, \"VM\" as tournament,gruppe as groupletter,id,0 as pot,matches,win,draw,lose,points,goalf,goala,pos,land as name,fork as country,0 as teamcoef,0 as ccoef from stilling where aar=2018 order by gruppe,pos";
  $res=Database::get_handle()->kquery($q);
  $team=$res->fetch_assoc();
  while (true) {
    if (!$team) break;
    $newgroup=new Group($team['season'],$team['tournament'],$team['groupletter']);
    for ($i=0;$i<4;$i++) {
      $newgroup->addteam($team);
      $team=$res->fetch_assoc();
    }
    $groups[]=$newgroup;
  }
  return($groups);
}

?>
